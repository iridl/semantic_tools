#!/usr/bin/perl

# check the last mod time of the svn repos. for projectframework
# and compare with the runproject projectcache.  If SVN is more recent
# than the projectcache, update the repository.
# run on gfs2geo1

use Time::Local;

# make sure that the java home, path, and classpath are synchronized

# $ENV{'JAVA_HOME'} = "/local05/opt/jdk1.6.0_21";
$ENV{'PATH'} = "$ENV{'JAVA_HOME'}/bin:$ENV{'PATH'}:/usr/local/datalib/semantic_tools/bin";
$ENV{'CLASS_PATH'} = "$ENV{'CLASSPATH'}:$ENV{'JAVA_HOME'}/lib:/usr/local/datalib/semantic_tools/bin";

# open file that has svn revision number
open REVNUM, "<","/home/datag/semantics/pfrev.txt" or die $!;
my @filelines = <REVNUM>;
chomp($filelines[0]);
print $filelines[0],"\n";
close REVNUM or die $!;

my $info = `svn info https://svn.iri.columbia.edu/dl/projectframework`;
print $info;
@infopart = split(/\n/,$info);

# look for Revision number 
foreach (@infopart) {
    if ($_ =~ m/Revision/) {
# split revision line
	@revpart = split(/:/,$_);
    }
    $pos++;
}

my $lockfile = "/home/datag/semantics/locks/projectframework.lock";

print "Last repos. Revision:\t",$filelines[0], "\n";
print "Last SVN:Revision\t",$revpart[1],  "\n";

if ( $revpart[1] > $filelines[0] ) {
    # check for lock file
    if (-e $lockfile) {
    # check for stale lock file
        if ( time - (stat($lockfile)) gt 3600.0 ) {
	    unlink($lockfile);
	   } else {
	    die "Update of projectframework RDF repositories prevented by projectframework.lock\n";
	   }
    } 
    # create the lock file, run the process to add the triples to repository, then remove lock
    system("touch /home/datag/semantics/locks/projectframework.lock");
    chdir("/home/datag/semantics");
    system("/bin/rm -rf projectcache/*");
    system("./runproject");
    system("/bin/rm /home/datag/semantics/locks/projectframework.lock");
    print "Updated repository\n";
# update revision number in pfrev.txt
    open REVNUM, ">","/home/datag/semantics/pfrev.txt" or die $!;
    print REVNUM $revpart[1];
    close REVNUM or die $!;
    
} else {
    print "No repository update needed\n";
}
