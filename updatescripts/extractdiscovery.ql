CONSTRUCT *
  FROM {dataset} rdf:type {term:SearchItem,iridl:Content};
	         rss:link {},
       {dataset} term:isDescribedBy {semantic} rdf:type {sclass} rdf:type {term:Facet}; rdfs:subClassOf {taxa} rdf:type {term:Taxa},
{semantic} rdf:type {taxa},
[ {semantic} rdfs:label {slabel} ],
[ {sclass} rdfs:label {clabel} ]
UNION
CONSTRUCT *
FROM
    {dataset} rdf:type {term:SearchItem,sclass},
	{sclass} rdfs:subClassOf {term:SearchItem},
	[{dataset} rdf:type {iridl:dataset}],
	[{dataset} rss:link {}],
	[{dataset} xhtml:canonical {}]
UNION
CONSTRUCT *
FROM 
    {dataset} dc:title {dlabel}
UNION
CONSTRUCT *
FROM 
    {dclass} iridl:members_label {dclabel}
UNION
CONSTRUCT *
FROM    {dataset} dc:description {descp}
UNION
CONSTRUCT *
FROM    {dataset} iridl:icon {icon} 
UNION
CONSTRUCT *
FROM    {dataset} dcterm:isPartOf {pdataset}
UNION
CONSTRUCT {pdataset} dcterm:hasPart {dataset}
FROM    {pdataset} rdf:type {term:SearchItem};
        dcterm:hasPart {dataset} rdf:type {term:SearchItem}
UNION
CONSTRUCT {pdataset} iridc:hasPart {dataset}
FROM    {pdataset} rdf:type {term:SearchItem};
        iridc:hasPart {dataset} rdf:type {term:SearchItem}
UNION
CONSTRUCT {pdataset} dcterm:isReplacedBy {dataset}
FROM    {pdataset} rdf:type {term:SearchItem};
        dcterm:isReplacedBy {dataset} rdf:type {term:SearchItem}
UNION
CONSTRUCT * FROM {dataset} iridl:last_modified {lmod}
UNION
CONSTRUCT * FROM {dataset} iridl:expires {exp}
UNION
CONSTRUCT *
FROM
	{prop} rdfs:isDefinedBy {<http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl>}, {} prop {}
UNION
CONSTRUCT *
FROM
    {subfile} rdfcache:last_modified {lastmod}
UNION
CONSTRUCT *
FROM
{alang} rdf:type {iridl:Language},
{alang} propa {},
{alangw} rdf:type {iridl:LangWeight},
{alangw} propb {}
USING NAMESPACE
    iridl = <http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#>,
    iridc = <http://iridl.ldeo.columbia.edu/ontologies/iridc.owl#>,
    iriref = <http://iridl.ldeo.columbia.edu/ontologies/ReferenceList.xml#>,
    rss = <http://purl.org/rss/1.0/>,
    dc = <http://purl.org/dc/elements/1.1/>,
    dcterm = <http://purl.org/dc/terms/>,
    term = <http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#>,
    xhtml = <http://www.w3.org/1999/xhtml/vocab#>,
   rdfcache = <http://iridl.ldeo.columbia.edu/ontologies/rdfcache.owl#>
