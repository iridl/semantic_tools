#!/bin/bash

cd $(dirname $0)
docker build -t semantic_tools-build .
docker run \
       --rm \
       -u $(id -u) \
       -v $PWD:/build/semantic_tools \
       semantic_tools-build \
       make clean tarball
