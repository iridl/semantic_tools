IRI DL Semantic Tools
*********************

* Prerequisites:

   * Docker

* To obtain the sources (including underlying submodules)::

   git clone --recursive git@bitbucket.org:iridl/semantic_tools.git

* To build a tarball for distribution::

   ./build.sh

* To run semantic tools::

   export PATH=~/semantic_tools/bin:$PATH
   JAVAOPTS=-Xmx4096m rdfcache
   xmlfromsesame

* On MacOSX Catalina:: 
 
   * 'brew install raptor'  (to install rapper)(as opposed to unpacking from the semantic_tools Downloads
   * Make sure that you build or download semantic_tools-1.3.8-13 or higher

* Subdirs 'rdfcache' and 'xmldocs' have their own specific README files
    
